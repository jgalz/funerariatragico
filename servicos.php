<?php require 'views/header.php'; ?>

<!-- Serviços section -->
<div class="">
<section class="">
    <div class="container bg servicos">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h1 class="text-center mt-5">Serviços</h1>
                
                <ul class="list-unstyled">
                    <li>Translado nacional e internacional.</li>
                    <li>Tanatopraxia e Embalsamamento.</li> 
                    <li>Ornamentação de urna com flores naturais ou artificiais.</li>
                    <li>Confecção de coroa de flores naturais e artificiais.</li>
                    <li>Arranjos de capela.</li>
                    <li>Arranjo de livros de presença.</li> 
                    <li>Serviço de cremação.</li>
                    <li>Carro fúnebre especial para translado.</li> 
                    <li>Acompanhamento familiar com documentação.</li>
                    <li>Locação de capela.</li>
                    <li>Serviço de copa.</li>
                    <li>Placas de Bronze e granito.</li>
                    <li>Cerimonial de Luto.</li> 
                    <li>Santinhos para Missa de Sétimo Dia.</li>  
                </ul>
            </div>
        </div>
    </div>
</section>
</div>

<?php require 'views/footer.php'; ?>