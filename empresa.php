<?php require 'views/header.php'; ?>

<!-- Sobre section -->
<div class="">
<section class="empresa-sobre">
    <div class="container bg">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h1 class="text-center mt-5">FUNERÁRIA TRÁGICO</h1>
                <p>A Funerária Trágico atua em um dos momentos mais delicados da história de uma família, a perda de um ente querido. Para minimizar o sofrimento dos familiares que se encontram enlutados, oferecemos um serviço que atenda a todas as necessidades da família na hora do funeral, sem burocracia e com máxima agilidade.</p>

                <h2 class="mt-3">Missão</h2>
                <p>Prestar serviços funerários com profissionalismo, competência e sensibilidade.</p>

                <h2 class="mt-3">Visão</h2>
                <p>É possível ser profissional e trabalhar de forma solidária para mitigar o sofrimento dos envolvidos.</p>

                <h2 class="mt-3">Valores</h2>
                <ul class="list-group mb-5">
                    <li class="list-group-item">Agilidade.</li>
                    <li class="list-group-item">Descrição.</li>
                    <li class="list-group-item">Respeito pelo momento.</li>
                    <li class="list-group-item">Sentimento humanitário.</li>
                    <li class="list-group-item">Respeito pela dignidade do falecido(a).</li>
                    <li class="list-group-item">Profissionalismo.</li>
                    <li class="list-group-item">Eficiência.</li>
                    <li class="list-group-item">Solidariedade ao sofrimento.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
</div>

<?php require 'views/footer.php'; ?>