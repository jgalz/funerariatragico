<?php require 'views/header.php'; ?>

<!-- Serviços section -->
<div class="">
<section class="">
    <div class="container bg contatos">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h1 class="text-center mt-5">Contato</h1>
                <p class="mt-3"> Preencha o formulário que a Funerária Trágico entrará em contato com você o mais rápido possível.</p>
            </div>
        </div>
    

    <div class="row justify-content-lg-center">                  
        <div class="col-lg-6">
        <form action="">
            <div class="form-group">
                <label for="">Nome completo:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Telefone - (DDD+número):</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Celular - (DDD+número):</label>
                <input class="form-control" type="text" name="nome">

                <label for="">E-mail:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Endereço:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Cidade:</label>
                <input class="form-control" type="text" name="nome">

                <label for="">Mensagem</label>
                <textarea class="form-control" name="" id="" cols="10" rows="5"></textarea>

                <button class="btn bordo text-white mt-3">Enviar</button>
            </div>
        </form>
        </div>
    </div>
    </div>
</section>
</div>

<?php require 'views/footer.php'; ?>