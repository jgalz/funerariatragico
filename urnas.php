<?php require 'views/header.php'; ?>

<!-- Urnas section -->
<div class="">
<section class="urnas">
    <div class="container bg">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h1 class="text-center mt-5 mb-5">Modelos de Urnas</h1>
            </div>
        </div>
        <div class="row justify-content-lg-center">
            
                <div class="col-lg-3 col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c1.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 col-md-6 mt-3">
                    <h2>Modelo: Ref. 07</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c2.jpg" alt="">
                </div>    
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3">
                    <h2>Modelo: Ref. 08</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                 

                <div class="col-lg-3 col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c3.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3">
                    <h2>Modelo: Ref. 09</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                       

                <div class="col-lg-3  col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c4.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3">
                    <h2>Modelo: Ref. 10</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                

                <div class="col-lg-3  col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c5.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3">
                    <h2>Modelo: Ref. 11</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                
                
            


                <div class="col-lg-3  col-md-6 col-sm-12 mt-3">
                    <img src="imgs/c6.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3">
                    <h2>Modelo: Ref. 12</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                
            
                <div class="col-lg-3  col-md-6 col-sm-12 mt-3 mb-5">
                    <img src="imgs/c7.jpg" alt="">
                </div>
                <div class="col-lg-8  col-md-6 col-sm-12 mt-3 mb-5">
                    <h2>Modelo: Ref. 13</h2>
                    <h4>Descrição</h4>
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. In elementis mé pra quem é amistosis quis leo. Leite de capivaris, leite de mula manquis sem cabeça. Aenean aliquam molestie leo, vitae iaculis nisl.</p>
                </div>
                
                
            

        </div>
    </div>
</section>
</div>

<?php require 'views/footer.php'; ?>