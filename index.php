<?php require 'views/header.php'; ?>

<!-- Carousel -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="imgs/banner.jpg" height="550" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="imgs/Florr.jpg" height="550" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="imgs/benjamin.jpg" height="550" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!-- Sobre section -->
<section class="sobre-index bordo text-white">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-9 ">
                <h1 class="text-center">FUNERÁRIA TRÁGICO</h1>
                <p>A Funerária Trágico atua em um dos momentos mais delicados da história de uma família, a perda de um ente querido. Para minimizar o sofrimento dos familiares que se encontram enlutados, oferecemos um serviço que atenda a todas as necessidades da família na hora do funeral, sem burocracia e com máxima agilidade.</p>
                <hr>
            </div>
        </div>
    </div>
</section>

<!-- Produtos section -->
<section class="produtos-index bordo text-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <a href="urnas.php">
                        <img class="w-100" src="imgs/urna01.jpg" alt="Urnas">
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <a href="acessorios.php">
                        <img class="w-100" src="imgs/coroas01.jpg" alt="Coroas">
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <a href="acessorios.php">
                        <img class="w-100" src="imgs/arranjos02.jpg" alt="Arranjos">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require 'views/footer.php'; ?>