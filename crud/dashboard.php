<!doctype html>
<?php
  include ("header.php");
    session_start();
 
    if(isset($_SESSION['User'])) {
        echo ' 
        <div class="container mb-5"> Bem vindo:  ' . $_SESSION['User'].'<br/>';
        echo '<a href="../index.php">Logout</a> 
        </div>';
    }
    else{
        header("location:../index.php");
    }

    include ("conexao.php");  
    
    $grupo = selectAllPessoa();
?>
<!-- 1parte -->
 
    <div class="container">
      <h1>Clientes Cadastrados</h1>

      <a href="inserir.php">Cadastrar Clientes</a>
      <!-- <a href="../index.php">Site</a> -->
    </div>
    
    <br><br><br>
    
<div class="">
    <div class="container">
        <table class="table table-dark table-bordered">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Nascimento</th>
              <th scope="col">Endereço</th>
              <th scope="col">Telefone</th>
              <th scope="col">Editar</th>
              <th scope="col">Excluir</th>
            </tr>
          </thead>
          <tbody>
           
           <?php
              //mostrar na tela a tabela com os nomes da agenda
              // já cadastrados
            foreach ($grupo as $pessoa) {  ?>
                
            <tr>
              <td><?=$pessoa["nome"]?></td>
              <td><?= formatoData($pessoa["nascimento"]) ?></td>
              <td><?=$pessoa["endereco"]?></td>
              <td><?=$pessoa["telefone"]?></td>
              <td>
                <form action="alterar.php" method="POST" name="alterar">
                      <input type="hidden" name="id" value="<?=$pessoa["id"]?>">
                      <input class="btn btn-primary" type="submit" name="eidtar" value="Editar">
                </form>
              </td>
              <td>
                <form action="conexao.php" method="POST" name="excluir">
                      <input type="hidden" name="id" value="<?=$pessoa["id"]?>">
                      <input type="hidden" name="acao" value="excluir">
                      <input class="btn btn-danger" type="submit" name="excluir" value="Excluir">
                </form>
              </td>
            </tr>
            
            <?php }
              
           
            ?>

          </tbody>
        </table>
    </div>
</div>
  
<?php 

    function formatoData($data) {
        $array = explode("-", $data);
        // $data = 2018-04-14
        // $array[0] = 2018, $array[1] = 04, $array[2] = 14;
        $novaData = $array[2]."/".$array[1]."/".$array[0];
        return $novaData;
    }  
      
?>
   
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>