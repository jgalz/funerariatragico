<?php
//4 parte
include("conexao.php");
include ("header.php");
$pessoa = selectIdPessoa($_POST["id"]);
//var_dump($pessoa);

?>

    <div class="container">
        <h2 class="mt-5">Alterar Dados do Cliente</h2>
    
    <form name="dadosPessoa" action="conexao.php" method="POST">
        <table class="table table-dark table-bordered">
          <tbody>
            <tr>
              <td>Nome</td>
              <td><input type="text" name="nome" value="<?=$pessoa["nome"]?>" size="20"></td>
            </tr>
            <tr>
              <td>Nascimento</td>
              <td><input type="date" name="nascimento" value="<?=$pessoa["nascimento"] ?>" size="20"></td>
            </tr>
            <tr>
              <td>Telefone</td>
              <td><input type="text" name="telefone" value="<?=$pessoa["telefone"] ?>" size="20"></td>
            </tr>
            <tr>
              <td>Endereço</td>
              <td><input type="text" name="endereco" value="<?=$pessoa["endereco"] ?>" size="20"></td>
            </tr>
            <tr>
              <td>
              <input type="hidden" name="acao" value="alterar">
              <input type="hidden" name="id" value="<?=$pessoa["id"] ?>">
              </td>
              <td><button class="btn btn-danger" type="submit" value="Enviar" name="Enviar">Enviar</button></td>
            </tr>
          </tbody>
        </table>
    </form>  
    </div>
        
