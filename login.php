<?php require 'views/header.php'; 

?>

<!-- Login section -->
<section class="login-section">
<div class="container bg">
    <div class="row justify-content-lg-center">
        <div class="col-lg-6 mt-5">
            <p><a class="btn bordo text-white log" data-toggle="collapse" href="#multiCollapseExample1"role="button"aria-expanded="false" aria-controls="multiCollapseExample1">Login</a>
            </p>
            <div class="row">
                <div class="col">
                    <div class="collapse multi-collapse" id="multiCollapseExample1">
                        <div class="card card-body">
                            <form action="login/process.php" method="POST">
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input class="form-control" type="text" name="UName">

                                    <label for="">Senha:</label>
                                    <input class="form-control" type="password" name="Password">

                                    <button type="submit" name="Login" class="btn bordo text-white mt-3">Logar</button>
                                </div>
                            </form>
                        </div>
                    </div>

               
            </div>

        </div>
        </div>
    </div>
</div>
</section>

<?php require 'views/footer.php'; ?>