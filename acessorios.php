<?php require 'views/header.php'; ?>

<!-- Acessorios section -->
<div class="">
<section class="">
    <div class="container bg acessorios">
        <div class="row justify-content-lg-center">
            <div class="col-lg-9">
                <h1 class="text-center mt-5">Produtos</h1> 
            </div>
        </div>

        <div class="row justify-content-lg-center">
            <div class="col-lg-3 mb-5 mt-5">
                <div class="card">
                    <a href="">
                        <img class="card-img-top w-100" src="imgs/arranjo.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Arranjos</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 mb-5 mt-5">
                <div class="card">
                    <a href="">
                        <img class="card-img-top w-100" src="imgs/coroa.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Coroas</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 mb-5 mt-5">
                <div class="card">
                    <a href="">
                        <img class="card-img-top w-100" src="imgs/Velorio_1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Ornamentação</h5>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-lg-3 mb-5 mt-5">
                <div class="card">
                    <a href="">
                        <img class="card-img-top w-100" src="imgs/livro.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Livro de Presença</h5>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>
</div>

<?php require 'views/footer.php'; ?>